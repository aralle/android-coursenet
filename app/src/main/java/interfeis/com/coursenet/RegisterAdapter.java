package interfeis.com.coursenet;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RegisterAdapter extends RecyclerView.Adapter<DataCardViewHolder> {

    ArrayList<Register> datas;

    @NonNull
    @Override
    public DataCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View vw = LayoutInflater.from( parent.getContext() )
                .inflate( R.layout.card_data, parent, false );

        DataCardViewHolder dcv = new DataCardViewHolder(vw);

        return dcv;
    }

    @Override
    public void onBindViewHolder(@NonNull DataCardViewHolder holder, int position) {

        Register data = datas.get(position);
        holder.tvCardName.setText(data.nama);
        holder.tvCardPhone.setText(data.telephone);
        holder.hdnID.setText(data.id + "");

        Picasso.get()
                .load( Setting.get_url_server() + "images/" + data.profilePicture )
                .error(R.drawable.ic_videocam_black_24dp)
                .into(holder.imgProfilePic);


    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
