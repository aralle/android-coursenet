package interfeis.com.coursenet;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {


    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inf = inflater.inflate(R.layout.fragment_home, container, false);

        TextView tvhome = (TextView) inf.findViewById(R.id.tvfragmenthome);

        SharedPreferences spData = getActivity().getSharedPreferences("DATA_APP", MODE_PRIVATE);

        String emailData = (String) spData.getString("data_email", getString(R.string.user));

        tvhome.setText( emailData );
        // Inflate the layout for this fragment
        return inf;
    }

}
