package interfeis.com.coursenet;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    ActionBarDrawerToggle left_menu_toggle;

    boolean clickExit = false;

    SharedPreferences spData;

    /* For scanner/qr result */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult qrresult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if(qrresult != null){
            String qrResultString = qrresult.getContents();

            Toast.makeText(getApplicationContext(), qrResultString, Toast.LENGTH_LONG).show();
        }
    }

    /* activating double press to exit */
    @Override
    public void onBackPressed() {

        if( getSupportFragmentManager().getBackStackEntryCount() > 0 ){

            getSupportFragmentManager().popBackStackImmediate();
            return;

        }

        if(clickExit==true){
            super.onBackPressed();
            finishAffinity();
            return;
        }
        clickExit = true;

        Toast.makeText(getApplicationContext(), getString(R.string.press_again_to_exit), Toast.LENGTH_LONG).show();

        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                clickExit = false;
            }
        }, 3000);

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        left_menu_toggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        openAppFragment(new HomeFragment());

        TextView welcome = (TextView) findViewById(R.id.tvwelcome);

        spData = getSharedPreferences("DATA_APP", MODE_PRIVATE);

        final DrawerLayout main_act_layout = (DrawerLayout) findViewById(R.id.main_activity_layout);

        NavigationView left_menu = (NavigationView) findViewById(R.id.left_menu);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        left_menu_toggle = new ActionBarDrawerToggle(
                MainActivity.this,
                main_act_layout,
                (R.string.open),
                (R.string.close)
        );

        main_act_layout.addDrawerListener(left_menu_toggle);

        left_menu.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                        if(item.getItemId() == R.id.left_menu_home){

                            openAppFragment(new HomeFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.home), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_about){

                            openAppFragment(new AboutFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.about), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_list){

                            openAppFragment(new ListFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.list_member), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_tab){

                            openAppFragment(new TabFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.tab_layout), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_table){

                            openAppFragment(new TableFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.table_layout), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_scanner){

                            new IntentIntegrator(MainActivity.this).initiateScan();

                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_gmap){

                            openAppFragment(new GoogleMapsFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.google_maps), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_progressdialogue){

                            ProgressDialog pd = new ProgressDialog( MainActivity.this );
                            pd.setTitle(getString(R.string.loading));
                            pd.setMessage(getString(R.string.please_wait));
                            pd.setIcon(R.drawable.ic_photo_camera_black_24dp);

                            pd.show();

                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_fabspeeddial){

                            openAppFragment( new SpeedDialFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.speed_dial), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_change_password){

                            openAppFragment(new ChangePasswordFragment());

                            Toast.makeText(getApplicationContext(), getString(R.string.change_password), Toast.LENGTH_SHORT).show();
                            main_act_layout.closeDrawers();

                        }else if(item.getItemId() == R.id.left_menu_logout){

                            removeSharedData();

                            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(i);

                            finish();
                        }

                        return false;
                    }
                }
        );

        //EditText welcome = (EditText) findViewById(R.id.txtwelcome);

        // Get value from previous intent or activity
        //String email = getIntent().getStringExtra("login_email");

        //Toast.makeText(getApplicationContext(), "Welcome " + email, Toast.LENGTH_LONG).show();
        //welcome.setText(email);


        String email = spData.getString("data_email", getString(R.string.user));

        //Toast.makeText(getApplicationContext(), "Welcome " + email, Toast.LENGTH_LONG).show();
        //welcome.setText(email);
    }

    public void removeSharedData(){
        getSharedPreferences("DATA_APP", MODE_PRIVATE).edit().clear().commit();
    }

    public void openAppFragment(Fragment f){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_main, f)
                .addToBackStack( null )
                .commit();
    }

    /* Creating the options menu */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // digunakan untuk menampilkan menu di activity ini
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* When Options menu selected */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // dijalankan ketika menunya di click
        if(item.getItemId() == R.id.options_menu_home){
            Toast.makeText(getApplicationContext(), getString(R.string.home), Toast.LENGTH_SHORT).show();
        }else if(item.getItemId() == R.id.options_menu_about){
            Toast.makeText(getApplicationContext(), getString(R.string.about), Toast.LENGTH_SHORT).show();
        }else if(item.getItemId() == R.id.options_menu_logout){
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(i);

            removeSharedData();

            finish();
        }else if(item.getItemId() == R.id.options_menu_exit){

            AlertDialog.Builder ab = new AlertDialog.Builder(MainActivity.this);

            ab.setTitle(R.string.exit_confirmation);
            ab.setMessage(R.string.are_you_sure);
            ab.setIcon(R.drawable.ic_email_black_24dp);

            ab.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finishAffinity();
                }
            });

            ab.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            ab.create().show();

        }

        left_menu_toggle.onOptionsItemSelected(item);

        return super.onOptionsItemSelected(item);
    }
}
