package interfeis.com.coursenet;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        Handler h = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences spData = getSharedPreferences("DATA_APP", MODE_PRIVATE);

                Intent i = null;
                if(spData.contains("data_email")){
                    i = new Intent(getApplicationContext(), MainActivity.class);
                }else{
                    i = new Intent(getApplicationContext(), LoginActivity.class);
                }

                startActivity(i);
                finish();
            }
        }, 3000);
        /*
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(i);
                    }
                }, 5000);
        */
    }
}
