package interfeis.com.coursenet;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChangePasswordFragment extends Fragment {

    TextInputEditText txtoldpassword;
    TextInputEditText txtnewpassword;
    TextInputEditText txtconfpassword;
    Button btnsubmit;

    public ChangePasswordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View thefragment = inflater.inflate(R.layout.fragment_change_password, container, false);

        txtoldpassword    = (TextInputEditText) thefragment.findViewById(R.id.txtoldpass);
        txtnewpassword    = (TextInputEditText) thefragment.findViewById(R.id.txtpass);
        txtconfpassword   = (TextInputEditText) thefragment.findViewById(R.id.txtconfirmpass);
        btnsubmit         = (Button) thefragment.findViewById(R.id.btnsubmit);

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmitButtonClicked( view );
            }
        });
        return thefragment;
    }

    public void onSubmitButtonClicked(View view){

        String strOldPassword = txtoldpassword.getText().toString();
        String strNewPassword = txtnewpassword.getText().toString();
        String strConfPassword = txtconfpassword.getText().toString();

        if(strNewPassword.length()==0){

            txtnewpassword.setError(getString(R.string.login_error_pass));

        }else if(strNewPassword.equals(strConfPassword)==false){

            txtconfpassword.setError(getString(R.string.password_must_be_same));

        }else{

            Toast.makeText( getActivity(), R.string.success, Toast.LENGTH_SHORT).show();
            Snackbar.make(view, R.string.success, Snackbar.LENGTH_SHORT).show();
        }

    }

}
