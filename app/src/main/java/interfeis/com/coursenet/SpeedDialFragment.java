package interfeis.com.coursenet;


import android.os.Bundle;
import android.support.design.internal.NavigationMenu;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import io.github.yavski.fabspeeddial.FabSpeedDial;
import io.github.yavski.fabspeeddial.SimpleMenuListenerAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class SpeedDialFragment extends Fragment {


    public SpeedDialFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_speed_dial, container, false);

        FabSpeedDial fsd_menu = (FabSpeedDial) view.findViewById( R.id.fsd_menu);
        fsd_menu.setMenuListener( new SimpleMenuListenerAdapter(){

            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                // TODO: Do something with yout menu items, or return false if you don't want to show them

                return true;
            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                //TODO: Start some activity
                return false;
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

}
