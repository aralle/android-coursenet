package interfeis.com.coursenet;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import de.hdodenhof.circleimageview.CircleImageView;

// java class untuk card_data.xml
public class DataCardViewHolder extends RecyclerView.ViewHolder {

    CircleImageView imgProfilePic;
    TextView tvCardName;
    TextView tvCardPhone;
    TextView hdnID;

    public DataCardViewHolder(final View itemView) {
        super(itemView);

        imgProfilePic   = (CircleImageView) itemView.findViewById( R.id.imgProfilePic );
        tvCardName      = (TextView) itemView.findViewById( R.id.tvCardName );
        tvCardPhone     = (TextView) itemView.findViewById( R.id.tvcardphone );
        hdnID           = (TextView) itemView.findViewById( R.id.hdnid );

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MainActivity ma = ( MainActivity ) itemView.getContext();

                String strID = hdnID.getText().toString();

                RegisterDetailFragment rdf = new RegisterDetailFragment();

                Bundle b = new Bundle();
                b.putString("id", strID );

                rdf.setArguments(b);

                ma.openAppFragment( rdf );
            }
        });
    }
}
