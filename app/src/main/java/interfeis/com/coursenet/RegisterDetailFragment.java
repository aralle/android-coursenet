package interfeis.com.coursenet;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterDetailFragment extends Fragment {

    TextInputEditText txtName;
    TextInputEditText txtPhone;
    TextInputEditText txtEmail;
    String theID;

    Button btnEdit;
    Button btnDelete;


    public RegisterDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View fview = inflater.inflate(R.layout.fragment_register_detail, container, false);

        txtName = (TextInputEditText) fview.findViewById(R.id.txtname);
        txtPhone = (TextInputEditText) fview.findViewById(R.id.txttelephone);
        txtEmail = (TextInputEditText) fview.findViewById(R.id.txtemail);

        btnEdit = (Button) fview.findViewById(R.id.btnedit);
        btnDelete = (Button) fview.findViewById(R.id.btndelete);

        theID = getArguments().getString("id");

        OkHttpClient okHC = new OkHttpClient();

        Request reqOK = new Request.Builder()
                .url(Setting.get_url_server() + "get_register_detail.php?id=" + theID)
                .build();

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.loading));
        pd.setMessage(getString(R.string.please_wait));
        pd.setCancelable(false);
        pd.show();

        okHC.newCall(reqOK).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        pd.dismiss();
                        Snackbar.make(fview, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();

                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String respString = response.body().string();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final JSONObject respJSON = new JSONObject(respString);


                            boolean result_ok = respJSON.getBoolean("result");

                            if (result_ok == true) {

                                JSONObject result_data = respJSON.getJSONObject("data");

                                txtName.setText(result_data.getString("nama"));
                                txtEmail.setText(result_data.getString("email"));
                                txtPhone.setText(result_data.getString("telephone"));

                                pd.dismiss();

                            } else {

                                String f = respJSON.getString("field");
                                String m = respJSON.getString("message");

                                Snackbar.make(fview, m, Snackbar.LENGTH_LONG).show();
                                pd.dismiss();
                            }


                        } catch (JSONException e) {

                            Log.e("Error Register Detail Fragment", Log.getStackTraceString(e));
                            e.printStackTrace();
                        }
                    }
                });


            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onEditButtonClicked(view);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onDeleteButtonClicked(view);

            }
        });
        return fview;
    }

    public void onEditButtonClicked(final View view) {


        OkHttpClient okHC = new OkHttpClient();

        String strName = txtName.getText().toString();
        String strEmail = txtEmail.getText().toString();
        String strPhone = txtPhone.getText().toString();

        if (strEmail.length() == 0) {

            txtEmail.setError(getString(R.string.login_error_email));

        } else if (strName.length() == 0) {

            txtName.setError(getString(R.string.register_error_name));

        } else {

            RequestBody okReqBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("id", theID)
                    .addFormDataPart("txtname", strName)
                    .addFormDataPart("txtemail", strEmail)
                    .addFormDataPart("txtphone", strPhone)
                    .build();

            Request okReq = new Request.Builder()
                    .url(Setting.get_url_server() + "update_register_detail.php")
                    .post(okReqBody)
                    .build();

            final ProgressDialog pd = new ProgressDialog(getActivity());
            pd.setTitle(getString(R.string.loading));
            pd.setMessage(getString(R.string.please_wait));
            pd.setCancelable(false);
            pd.show();

            okHC.newCall(okReq).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            pd.dismiss();
                            Snackbar.make(view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    final String respString = response.body().string();
                    Log.e("testing", respString);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                JSONObject respJSON = new JSONObject(respString);

                                boolean is_result_ok = respJSON.getBoolean("result");
                                String f = respJSON.getString( "field");
                                String m = respJSON.getString("message");

                                if( f.equals( "email" ) ) {
                                    txtEmail.requestFocus();
                                    txtEmail.setError(m);
                                }else if( f.equals( "fullname" ) ) {
                                    txtName.requestFocus();
                                    txtName.setError(m);
                                }else if( f.equals( "phone") ) {
                                    txtPhone.requestFocus();
                                    txtPhone.setError(m);
                                }else{
                                    Snackbar.make(view, m, Snackbar.LENGTH_LONG).show();
                                }
                                pd.dismiss();

                            } catch (JSONException e) {
                                Log.e("Error on edit button", Log.getStackTraceString(e));
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });

        }


    }


    public void onDeleteButtonClicked(final View view) {

        String theID = getArguments().getString("id");

        OkHttpClient okHC = new OkHttpClient();

        RequestBody okReqBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("id", theID)
                .build();

        Request okReq = new Request.Builder()
                .url(Setting.get_url_server() + "delete_register_detail.php")
                .post(okReqBody)
                .build();

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setTitle(getString(R.string.loading));
        pd.setMessage(getString(R.string.please_wait));
        pd.setCancelable(false);
        pd.show();


        okHC.newCall(okReq).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        pd.dismiss();
                        Snackbar.make(view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();

                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                final String respString = response.body().string();

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            JSONObject respJSON = new JSONObject(respString);

                            boolean is_result_ok = respJSON.getBoolean("result");
                            String d = respJSON.getString("data");
                            String m = respJSON.getString("message");

                            if (is_result_ok == true) {

                                Snackbar.make(view, m, Snackbar.LENGTH_LONG).show();

                                getActivity().getSupportFragmentManager().popBackStackImmediate();

                                pd.dismiss();

                            } else {

                                Snackbar.make(view, m, Snackbar.LENGTH_LONG).show();
                                pd.dismiss();

                            }

                        } catch (JSONException e) {
                            Log.e("Error Register Detail Fragment", Log.getStackTraceString(e));
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }

}
