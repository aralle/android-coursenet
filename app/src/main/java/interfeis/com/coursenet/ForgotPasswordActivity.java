package interfeis.com.coursenet;

import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

public class ForgotPasswordActivity extends AppCompatActivity {

    TextInputEditText txtEmail;
    TextInputEditText txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }

    public void btnForgotClicked(View view) {
        // 1. ambil edittext dari xmlnya

        txtEmail = (TextInputEditText) findViewById(R.id.txtemail);
        txtPass = (TextInputEditText) findViewById(R.id.txtpass);

        String strEmail = txtEmail.getText().toString();
        String strPass = txtPass.getText().toString();

        if (strEmail.length() == 0) {

            Toast.makeText(getApplicationContext(), R.string.login_error_email, Toast.LENGTH_SHORT).show();
            //txtEmail.setError(getString(R.string.login_error_email));
            //Snackbar.make(view, R.string.login_error_email, Snackbar.LENGTH_SHORT).show();

        } else if (strPass.length() == 0) {

            txtPass.setError(getString(R.string.login_error_pass));

        } else {
            Snackbar.make(view, R.string.success, Snackbar.LENGTH_SHORT).show();
        }
    }
}
