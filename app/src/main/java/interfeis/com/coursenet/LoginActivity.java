package interfeis.com.coursenet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity {

    String extrastring;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Snackbar.make( getWindow().getDecorView().getRootView(), extrastring, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if( getIntent().hasExtra("successmessage") ){
            extrastring = getIntent().getStringExtra("successmessage");
            if( extrastring.length() > 0 ){
                Toast.makeText( getApplicationContext(), extrastring, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void showMessage(final View view) {

        // 1. ambil edittext dari xmlnya

        final EditText txtEmail = (EditText) findViewById(R.id.txtemail);
        final EditText txtPass = (EditText) findViewById(R.id.txtpass);

        final String strEmail = txtEmail.getText().toString();
        final String strPass = txtPass.getText().toString();

        if (strEmail.length() == 0) {

            Toast.makeText(getApplicationContext(), R.string.login_error_email, Toast.LENGTH_SHORT).show();
            //txtEmail.setError(getString(R.string.login_error_email));
            txtEmail.requestFocus();

        } else if (strPass.length() == 0) {

            txtPass.setError(getString(R.string.login_error_pass));
            txtPass.requestFocus();

        } else {

            OkHttpClient okHC = new OkHttpClient();

            RequestBody okReqBody = new MultipartBody.Builder()
                                    .setType( MultipartBody.FORM )
                                    .addFormDataPart("txtemail", strEmail)
                                    .addFormDataPart("txtpassword", strPass)
                                    .build();

            Request okRequest = new Request.Builder()
                    .post( okReqBody )
                    .url( Setting.URL_SERVER + "proses_login.php" )
                    .build();

            final ProgressDialog pd = new ProgressDialog( LoginActivity.this );

            pd.setTitle( getString( R.string.loading ));
            pd.setMessage( getString( R.string.please_wait ) );
            pd.setCancelable( false );
            pd.setIcon(R.drawable.ic_home_black_24dp);

            pd.show();

            okHC.newCall( okRequest ).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            Snackbar.make(view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String responseString = null;
                            try {
                                responseString = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject responseJSON = new JSONObject( responseString );

                                boolean r = responseJSON.getBoolean( "result");

                                if( r == true ){

                                    pd.dismiss();

                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    i.putExtra("login_email", strEmail);
                                    startActivity(i);

                                    SharedPreferences.Editor spData = getSharedPreferences("DATA_APP", MODE_PRIVATE).edit();
                                    spData.putString("data_email", strEmail);
                                    spData.commit();

                                    finish();

                                } else {

                                    String f = responseJSON.getString("field");
                                    String m = responseJSON.getString( "message");

                                    if(f.equals("password")){
                                        txtPass.requestFocus();
                                        txtPass.setError(m);
                                    } else if(f.equals("email")){
                                        txtEmail.requestFocus();
                                        txtEmail.setError(m);
                                    }
                                    pd.dismiss();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                }
            });


        }
    }

    public void btnRegisterClicked(View view){

        Intent i = new Intent(getApplicationContext(), RegisterActivity.class);

        startActivity(i);
    }

    public void btnForgotClicked(View view) {

        Intent i = new Intent(getApplicationContext(), ForgotPasswordActivity.class);

        startActivity(i);
    }
}
