package interfeis.com.coursenet;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {


    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View inf = inflater.inflate(R.layout.fragment_list, container, false);
        // Inflate the layout for this fragment

        final RecyclerView rvList = (RecyclerView) inf.findViewById(R.id.rvlist);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        GridLayoutManager glm = new GridLayoutManager(getActivity(), 2);

        // rvList.setLayoutManager(llm);
        rvList.setLayoutManager(glm);

        OkHttpClient okHC = new OkHttpClient();
        Request okReq = new Request.Builder()
                .get()
                .url(Setting.URL_SERVER + "get_data.php")
                .build();


        final ProgressDialog pd = new ProgressDialog(getActivity());

        pd.setTitle(getString(R.string.loading));
        pd.setMessage(getString(R.string.please_wait));
        pd.setCancelable(false);
        pd.setIcon(R.drawable.ic_home_black_24dp);

        pd.show();

        okHC.newCall(okReq).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pd.dismiss();
                        Snackbar.make(inf, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                final String responseString = response.body().string();


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {

                            JSONObject objResponse = new JSONObject( responseString );

                            boolean r = objResponse.getBoolean("result");

                            if (r == true) {

                                JSONArray dataResponse = objResponse.getJSONArray("data");

                                RegisterAdapter adapter = new RegisterAdapter();
                                adapter.datas = new ArrayList<>();

                                for (int i = 0; i < dataResponse.length(); i++) {
                                    Register rObj = new Register();
                                    JSONObject dataObj = dataResponse.getJSONObject(i);

                                    rObj.nama           = dataObj.getString("nama");
                                    rObj.email          = dataObj.getString("email");
                                    rObj.telephone      = dataObj.getString( "telephone");
                                    rObj.id             = dataObj.getInt("id");
                                    rObj.profilePicture = dataObj.getString("profile_picture");

                                    adapter.datas.add(rObj);
                                }

                                rvList.setAdapter(adapter);
                                pd.dismiss();

                            } else {

                                String f = objResponse.getString("field");
                                String m = objResponse.getString("message");

                                pd.dismiss();

                                Snackbar.make(inf, m, Snackbar.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        });


        return inf;

    }

}
