package interfeis.com.coursenet;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment {


    public TabFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View a = inflater.inflate(R.layout.fragment_tab, container, false);

        TabLayout tab_parent = (TabLayout) a.findViewById(R.id.tab_parent);
        ViewPager tab_pager = (ViewPager) a.findViewById(R.id.tab_viewpager_one);

        tab_pager.setAdapter(new TabSetting(getChildFragmentManager()));
        tab_parent.setupWithViewPager(tab_pager);

        tab_parent.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        tab_parent.getTabAt(2).setIcon(R.drawable.ic_vpn_key_black_24dp);

        return a;
    }

    class TabSetting extends FragmentStatePagerAdapter{

        public TabSetting(FragmentManager fm) {
            super(fm);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {

            if(position==0){
                return getString(R.string.home);

            }else if(position==1){
                return getString(R.string.about);
            }

            return super.getPageTitle(position);
        }

        @Override
        public Fragment getItem(int position) {

            if(position==0){
                return new HomeFragment();
            }else if(position==1){
                return new AboutFragment();
            }else if (position==2){
                return new ChangePasswordFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

}
