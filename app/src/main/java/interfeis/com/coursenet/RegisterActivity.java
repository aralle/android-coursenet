package interfeis.com.coursenet;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText txtEmail;
    EditText txtPass;
    EditText txtName;
    EditText txtPhone;
    ImageView imgPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void btnSubmitClicked(final View view) {

        // 1. ambil edittext dari xmlnya
        this.txtEmail = (EditText) findViewById(R.id.txtemail);
        this.txtPass = (EditText) findViewById(R.id.txtpassword);
        this.txtName = (EditText) findViewById(R.id.txtname);
        this.imgPhoto = (ImageView) findViewById(R.id.imgphoto);
        this.txtPhone = (EditText) findViewById(R.id.txtphone);


        String strEmail = this.txtEmail.getText().toString();
        String strPass = this.txtPass.getText().toString();
        String strName = this.txtName.getText().toString();
        String strPhone = this.txtPhone.getText().toString();

        String imgbase64 = "";

        if (imgPhoto.getDrawable() != null) {

            Bitmap bitImgPhoto = ((BitmapDrawable) this.imgPhoto.getDrawable()).getBitmap();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitImgPhoto.compress(Bitmap.CompressFormat.JPEG, 100, baos);

            byte[] hasilConvert = baos.toByteArray();
            imgbase64 = Base64.encodeToString(hasilConvert, Base64.NO_WRAP);
        }


        if (strEmail.length() == 0) {

            Toast.makeText(getApplicationContext(), R.string.login_error_email, Toast.LENGTH_SHORT).show();
            //txtEmail.setError(getString(R.string.login_error_email));

        } else if (strPass.length() == 0) {

            txtPass.setError(getString(R.string.login_error_pass));

        } else if (strName.length() == 0) {

            txtName.setError(getString(R.string.register_error_name));

        } else {

            // 1. set the OkHttpClient variable
            OkHttpClient phpserver = new OkHttpClient();

            // 2. set the request
            RequestBody reqBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("txtfullname", strName)
                    .addFormDataPart("txtemail", strEmail)
                    .addFormDataPart("txtpassword", strPass)
                    .addFormDataPart("imgphoto", imgbase64)
                    .addFormDataPart( "txtphone", strPhone )
                    .build();

            // 3. set the url address
            Request request = new Request.Builder()
                    .post(reqBody)
                    .url(Setting.URL_SERVER + "proses_register.php")
                    .build();

            // 4. show a progress dialog
            final ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
            pd.setTitle("Loading...");
            pd.setMessage("Please Wait...");
            pd.setCancelable(false);
            pd.setIcon(R.drawable.ic_home_black_24dp);

            pd.show();

            // 5. Send the data
            phpserver.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            Snackbar.make(view, getString(R.string.cannot_connect_server), Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    });

                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            String resresult = null;
                            try {
                                resresult = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            try {
                                JSONObject resultjson = new JSONObject(resresult);
                                boolean r = resultjson.getBoolean("result");

                                if (r == true) {

                                    pd.dismiss();

                                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                    i.putExtra("successmessage", getString(R.string.you_registered_now));
                                    startActivity(i);

                                    finish();

                                } else {
                                    String f = resultjson.getString("field");
                                    String m = resultjson.getString("message");

                                    if (f.equals("fullname")) {
                                        txtName.requestFocus();
                                        txtName.setError(m);
                                    } else if (f.equals("password")) {
                                        txtPass.requestFocus();
                                        txtPass.setError(m);
                                    } else if (f.equals("email")) {
                                        txtEmail.requestFocus();
                                        txtEmail.setError(m);
                                    } else if( f.equals("phone")){
                                        txtPhone.requestFocus();
                                        txtPhone.setError(m);
                                    }
                                    pd.dismiss();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            });
        }
    }

    public void btnPhotoClicked(View view) {
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(i, 8888);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 8888) {

            if (resultCode == RESULT_OK) {
                this.imgPhoto = (ImageView) findViewById(R.id.imgphoto);
                Bitmap theimage = (Bitmap) data.getExtras().get("data");
                this.imgPhoto.setImageBitmap(theimage);
            }

        } else if (requestCode == 9999) {

        }
    }

    public void btnVideoClicked(View view) {
        Intent i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        startActivityForResult(i, 9999);

    }
}
